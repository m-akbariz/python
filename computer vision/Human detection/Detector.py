import cv2
import numpy as np
import glob
from sklearn import svm
import itertools
import  imutils
import pickle
from imutils.object_detection import non_max_suppression

hog = cv2.HOGDescriptor('1.xml')
list1 = []
Y = []
X = []
test = []
fnames1 = glob.glob('INRIAPerson/96X160H96/Train/pos/*.png')

for fname in fnames1:
    print "fname pos"
    I1 = cv2.imread(fname)
    I1 = I1[16:-16, 16:-16, :]
    feature = hog.compute(I1)
    list1.append(feature)
    Y.append(1)

fnames2 = glob.glob('./INRIAPerson/neg/*.png')

for fname in fnames2:
    I1 = cv2.imread(fname)
    samples1 = np.random.randint(0, I1.shape[1] - 64, 10)
    samples2 = np.random.randint(0, I1.shape[0] - 128, 10)
    samples = zip(samples1, samples2)
    for item in samples:
        I2 = I1[item[1]:item[1] + 128, item[0]:item[0] + 64, :]
        feature = hog.compute(I2)
        list1.append(feature)
        Y.append(0)

print list1.__len__()
X = np.asarray(list1, dtype=np.float64)
Y = np.asarray(Y)
print X.shape
X = np.reshape(X, (X.shape[0], X.shape[1]))
print X.shape
# print X
# print list1.__len__()
# print X
#
# # print Y
#
clf = svm.SVC(kernel='sigmoid', gamma='auto', C=5, max_iter=-1, tol=1e-4, coef0=1)
clf.fit(X, Y)
print clf.score(X, Y)



listTest = []
YTest = []
testImages = glob.glob('INRIAPerson/pos/*.png')
for fname in testImages:
    Itp = cv2.imread(fname)
    Itp = Itp[3:-3, 3:-3, :]
    feature = hog.compute(Itp)
    listTest.append(feature)
    YTest.append(1)

testImages = glob.glob('INRIAPerson/neg/*.png')

for fname in testImages:
    Itn = cv2.imread(fname)
    samples1 = np.random.randint(0, Itn.shape[1] - 64, 10)
    samples2 = np.random.randint(0, Itn.shape[0] - 128, 10)
    samples = zip(samples1, samples2)
    for item in samples:
        I2 = Itn[item[1]:item[1] + 128, item[0]:item[0] + 64, :]
        feature = hog.compute(I2)
        listTest.append(feature)
        YTest.append(0)

Xtest = np.asarray(listTest, dtype=np.float64)
Xtest = np.reshape(Xtest, (Xtest.shape[0], Xtest.shape[1]))
YTest = np.array(YTest)
print clf.score(Xtest,YTest)


#
# Itest1 = cv2.imread('7.png')
# x = np.random.randint(0, Itest1.shape[1] - 64, 1)
# y = np.random.randint(0, Itest1.shape[0] - 128, 1)
# point = zip(x, y)
# I3 = Itest1[0:128, 0:64, :]
# feature1 = hog.compute(I3)

# Itest2 = cv2.imread('6.png')
# Itest2 = Itest2[3:3, 16:-16, :]
# feature2 = hog.compute(Itest2)

# # test.append(feature)
# # # test = np.asfarray(test)
# # # test = np.reshape(test, (1, X.shape[1]))
# # print test.shape


# testing :
# scale = 1.1
# padding = (8,8)
# winStride = (4,4)
# supportVectors = []
# supportVectors.append(np.dot(clf.dual_coef_, clf.support_vectors_)[0])
# supportVectors.append([clf.intercept_])
# supportVectors = list(itertools.chain(*supportVectors))
# hog.setSVMDetector(np.array(supportVectors, dtype=np.float64))
# image = cv2.imread('7.png')
# orig = image.copy()
# (rects, weights) = hog.detectMultiScale(image, winStride=winStride, padding=padding, scale=scale)
# for (x, y, w, h) in rects:
#     cv2.rectangle(orig, (x, y), (x + w, y + h), (0, 0, 255), 2)
#     print x ,y , w, h
# rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rects])
# pick = non_max_suppression(rects, probs=None, overlapThresh=0.65)
# for (xA, yA, xB, yB) in pick:
#     cv2.rectangle(image, (xA, yA), (xB, yB), (0, 255, 0), 2)
# # cv2.imshow("Before NMS", orig)
# cv2.imshow("After NMS", image)
#
# image = cv2.imread('6.png')
# orig = image.copy()
# (rects, weights) = hog.detectMultiScale(image, winStride=winStride, padding=padding, scale=scale)
# for (x, y, w, h) in rects:
#     cv2.rectangle(orig, (x, y), (x + w, y + h), (0, 0, 255), 2)
#     print x ,y , w, h
# rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rects])
# pick = non_max_suppression(rects, probs=None, overlapThresh=0.65)
# for (xA, yA, xB, yB) in pick:
#     cv2.rectangle(image, (xA, yA), (xB, yB), (0, 255, 0), 2)
# # cv2.imshow("Before NMS2", orig)
# cv2.imshow("After NMS2", image)
#
# image = cv2.imread('8.png')
# orig = image.copy()
# (rects, weights) = hog.detectMultiScale(image, winStride=winStride, padding=padding, scale=scale)
# for (x, y, w, h) in rects:
#     cv2.rectangle(orig, (x, y), (x + w, y + h), (0, 0, 255), 2)
#     print x ,y , w, h
# rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rects])
# pick = non_max_suppression(rects, probs=None, overlapThresh=0.65)
# for (xA, yA, xB, yB) in pick:
#     cv2.rectangle(image, (xA, yA), (xB, yB), (0, 255, 0), 2)
# # cv2.imshow("Before NMS3", orig)
# cv2.imshow("After NMS3", image)
#
# image = cv2.imread('10.png')
# orig = image.copy()
# (rects, weights) = hog.detectMultiScale(image, winStride=winStride, padding=padding, scale=scale)
# for (x, y, w, h) in rects:
#     cv2.rectangle(orig, (x, y), (x + w, y + h), (0, 0, 255), 2)
#     print x ,y , w, h
# rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rects])
# pick = non_max_suppression(rects, probs=None, overlapThresh=0.65)
# for (xA, yA, xB, yB) in pick:
#     cv2.rectangle(image, (xA, yA), (xB, yB), (0, 255, 0), 2)
# # cv2.imshow("Before NMS4", orig)
# cv2.imshow("After NMS4", image)
#
# image = cv2.imread('11.png')
# orig = image.copy()
# (rects, weights) = hog.detectMultiScale(image, winStride=winStride, padding=padding, scale=scale)
# for (x, y, w, h) in rects:
#     cv2.rectangle(orig, (x, y), (x + w, y + h), (0, 0, 255), 2)
#     print x ,y , w, h
# rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rects])
# pick = non_max_suppression(rects, probs=None, overlapThresh=0.65)
# for (xA, yA, xB, yB) in pick:
#     cv2.rectangle(image, (xA, yA), (xB, yB), (0, 255, 0), 2)
# # cv2.imshow("Before NMS5", orig)
# cv2.imshow("After NMS5", image)
supportVectors = []
supportVectors.append(np.dot(clf.dual_coef_, clf.support_vectors_)[0])
supportVectors.append([clf.intercept_])
supportVectors = list(itertools.chain(*supportVectors))
hog.setSVMDetector(np.array(supportVectors, dtype=np.float64))
for fname in glob.glob('INRIAPerson/my_test/*'):
    Itest = cv2.imread(fname)
    Itest = imutils.resize(Itest,width=min(400,Itest.shape[1]))
    #Itest = Itest[16:-16,16:-16,:]
    #Ty = cv2.resize(Itest, (64, 128), interpolation=cv2.INTER_AREA)
    #G = cv2.cvtColor(Ty, cv2.COLOR_BGR2GRAY)
    o = Itest.copy()
    # detect people in the image
    rects, weights = hog.detectMultiScale(Itest, winStride=(1,2),scale=1.4 , padding=(8,8),
                                          finalThreshold=100 , useMeanshiftGrouping=0)
    # print rects

    for (x, y, w, h) in rects:
        cv2.rectangle(o, (x, y), (x + w, y + h), (0, 255, 255),2)


    rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rects])
    pick = non_max_suppression(rects, probs=None, overlapThresh=0.65)


    for (xA, yA, xB, yB) in pick:
        cv2.rectangle(Itest, (xA, yA), (xB, yB), (0, 255, 0), 2)

    cv2.imshow("Before NMS", o)
    if cv2.waitKey(0) & 0xFF == ord('q'):
        break
    cv2.imshow("After NMS", Itest)
    if cv2.waitKey(0) & 0xFF == ord('q'):
        break



#



# print clf.predict(feature1.T)
# print clf.score(feature1.T, [1])
# print clf.predict(feature2.T)f
# f_myfile = open('svm.pickle', 'wb')
# pickle.dump(clf, f_myfile)
# f_myfile.close()


# thefile = open('test.txt', 'w')
# for item in clf.support_:
#   print>>thefile, item,'\n'
# print list
# print I
# I = I [16:144,16:80]
# cv2.imshow('re',I1)
# fea = hog.compute(I1)
# print fea.size
cv2.waitKey(0)
