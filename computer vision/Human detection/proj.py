import numpy as np
from sklearn import svm
import glob
import cv2
import imutils

h = []
tags = []
clf = svm.SVC(kernel='sigmoid', gamma='auto', C=5, max_iter=-1, tol=1e-4, coef0=1)
hog = cv2.HOGDescriptor()
hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())

i = 1
pos_files = glob.glob('../INRIAPerson/96X160H96/Train/pos/*.png')
neg_files = glob.glob('../INRIAPerson/neg/*.png')
test_files = glob.glob('../INRIAPerson/my_test/*.png')
for f in neg_files:
    I = cv2.imread(f)
    y = np.random.randint(0, I.shape[0] - 128, 10)
    x = np.random.randint(0, I.shape[1] - 64, 10)
    samples = zip(x, y)
    for a,b in samples:
        I1 = I[b:b + 128, a:a + 64, :]
        U = hog.compute(I1)
        h.append(U)
        tags.append(0)
		print('image',i,'added')
print "neg shape"

for f in pos_files:
    I1 = cv2.imread(f)
    # o = cv2.imread(f)
    I1 = I1[16:-16, 16:-16, :]
    # cv2.imshow('test', o)
    desc = hog.compute(I1)
    h.append(desc)
    tags.append(1)

print "pos shape"

h = np.asarray(h, dtype=np.float64)
tags = np.asarray(tags)
h = np.reshape(h, (h.shape[0], h.shape[1]))
print(h.shape)
print(h.dtype)
# print(h[0])
print(tags.shape)
print(tags.dtype)
# print(tags[0])
clf.fit(h, tags)
print "fit done"
xtest = []
ytest = []
for f in test_files:
    it = cv2.imread(f)
    # it = it[3:-3, 3:-3, :]
    # it = cv2.resize(it, (128, 64))
    o = it.copy()
    rects, weights = hog.detectMultiScale(it, winStride=(5, 5), scale=1.05, padding=(0, 0),
                                          finalThreshold=10, useMeanshiftGrouping=0)
    for (x, y, w, h) in rects:
        cv2.rectangle(o, (x, y), (x + w, y + h), (0, 255, 255), 2)
    cv2.imshow('test', o)
    cv2.waitKey(0)
    des = hog.compute(it)
    xtest.append(des)
    ytest.append(1)

    print(clf.predict(des.T))
    print(clf.score(des, 1))
    # cv2.waitKey(0)

# xtest = np.array(xtest, dtype=np.float64)
# xtest = np.reshape(xtest, (xtest.shape[0], xtest.shape[1]))
# ytest = np.array(ytest)
cv2.waitKey(0)
# print(clf.predict(xtest))
