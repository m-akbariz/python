# import the necessary packages
import numpy as np
import imutils
import cv2


def stitch(images, ratio=0.75, reprojThresh=4.0):

	(imageB, imageA) = images
	sift = cv2.xfeatures2d.SIFT_create()
	(kpsA, featuresA) = sift.detectAndCompute(imageA,None)
	(kpsB, featuresB) = sift.detectAndCompute(imageB,None)
	kpsA = np.float32([kp.pt for kp in kpsA])
	kpsB = np.float32([kp.pt for kp in kpsB])
	#source:
	# https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_feature2d/py_feature_homography/py_feature_homography.html

	M = matchKeypoints(kpsA, kpsB,	featuresA, featuresB, ratio, reprojThresh)

	

	
	if M is None:
		return None


	(matches, H, status) = M
	result = cv2.warpPerspective(imageA, H,(imageA.shape[1] + imageB.shape[1], imageA.shape[0]))
	result[0:imageB.shape[0], 0:imageB.shape[1]] = imageB
	t = 0
	k = imageA.shape[1] + imageB.shape[1]-1
	kk = 0;
	for i in range(k,0,-1):
		if(result[0][i].all()!=0):
			kk=i-1
			break
	print kk
	result = result[0::, 0:kk]
	print result.shape
	print imageA.shape
	print imageB.shape



	return result

def matchKeypoints(kpsA, kpsB, featuresA, featuresB,ratio, reprojThresh):
	matcher = cv2.BFMatcher()
	rawMatches = matcher.knnMatch(featuresA, featuresB, k=2)
	matches = []
	for m in rawMatches:
		if len(m) == 2 and m[0].distance < m[1].distance * ratio:
			matches.append((m[0].trainIdx, m[0].queryIdx))


	if len(matches) > 4:

		ptsA = np.array([kpsA[i] for (_, i) in matches])
		ptsB = np.array([kpsB[i] for (i, _) in matches])

		(H, status) = cv2.findHomography(ptsA, ptsB, cv2.RANSAC,reprojThresh)

		return (matches, H, status)

	return None

# I1 = cv2.imread("KNTU-1.JPG")
# # cv2.imshow("kntu 1",I1)
# # cv2.waitKey(0)
# I2 = cv2.imread("KNTU-2.JPG")
# # cv2.imshow("kntu 2",I2)
# # cv2.waitKey(0)
# I3 = cv2.imread("KNTU-3.JPG")
# # cv2.imshow("kntu 3",I3)
# # cv2.waitKey(0)
# I4 = cv2.imread("KNTU-4.JPG")
# # cv2.imshow("kntu 4",I4)
# # cv2.waitKey(0)
# I5 = stitch((I1,I2))
# I5 = stitch((I5,I3))
# I5 = stitch((I5,I4))
# cv2.imshow("i5",I5)
# cv2.waitKey(0)

I1 = cv2.imread("Zabol-1.JPG")
# cv2.imshow("kntu 1",I1)
# cv2.waitKey(0)
I2 = cv2.imread("Zabol-2.JPG")
# cv2.imshow("kntu 2",I2)
# cv2.waitKey(0)
I3 = cv2.imread("Zabol-3.JPG")
# cv2.imshow("kntu 3",I3)
# cv2.waitKey(0)
I4 = cv2.imread("Zabol-4.JPG")
# cv2.imshow("kntu 4",I4)
# cv2.waitKey(0)
I5 = stitch((I1,I2))
I5 = stitch((I5,I3))
I5 = stitch((I5,I4))
# cv2.imwrite('originalpanorama.JPG',I1)
# I5 = cv2.resize(I5,(I5.shape[1]/2,I5.shape[0]/2))
# cv2.imwrite("resized panorama.JPG",I5)
cv2.imshow("i5",I5)
cv2.waitKey(0)