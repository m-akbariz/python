from numpy import *
import numpy as np


class slp:
    def __init__(self):
        random.seed(1)
        self.weights = random.random((3, 1)) * 2 - 1

    def sigmoid(self, x):
        return 1 / (1 + exp(-x))

    def sigmoid_derivative(self, x):
        return x * (1 - x)

    def conv(self, a, b):
        return a.T.dot(b)

    def output(self, args):
        return self.sigmoid(self.conv(args, self.weights))

    def error(self, out, des):
        return des - out

    def train(self, inputs, targets):
        for i in range(0,100):
            for mat, t in zip(inputs, targets):
                # print "t:",t
                # print "mat:", np.array([mat]).T
                out = self.output(mat)
                err = self.error(out, t)
                adj = np.array([np.dot(np.array([mat]).T, err * self.sigmoid_derivative(out))])
                self.weights += adj.T
                # print(err * self.sigmoid_derivative(out))
                # print(self.weights)
                # print("---")

        print(self.weights)


if __name__ == '__main__':
    test = slp()
    print ('weights before training:',test.weights)
    training_set_inputs = array([[0, 0, 1],
                                 [1, 1, 1],
                                 [1, 0, 1],
                                 [0, 1, 1]])
    training_set_outputs = array([[0, 1, 1, 0]]).T
    print("-----\n",'weights after training')
    test.train(training_set_inputs,training_set_outputs)
    print('output is:',test.output(np.array([1,0,0])))